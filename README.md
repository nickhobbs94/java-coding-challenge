## Liftango Coding Challenge

The goal of this assessment is to get an understanding of how you solve a new problem presented to you. We are also interested in your use of git to track your changes over time and how you test the functionality of the application.
You have as much time as you need to complete this, however we do not expect you to spend more than a couple of hours. 

### Setup
After cloning the repo from bitbucket you're expected to build the base application provided through [Gradle](https://gradle.org/).
We would suggest using [IntelliJ](https://www.jetbrains.com/idea/download/) to develop this program in. Community edition should be enough to achieve everything with this project. However you're free to use whatever tooling you like. As long as you're able to build and run the application / tests. 

#### Gradle
Included in this repo is the gradle wrapper script which allows you to build and run gradle without having it installed globaly. 

To build the application, run `./gradlew clean build`
To run the application, run `./gradlew run`


#### Docker
Additionally, we have prodivided [Dockerfile](Dockerfile) and a [docker-compose.yml](docker-compose.yml) if you wish to run and test your application in that way. 

In order to use it you have to build the project first: `./gradlew clean build` then build docker-image of the service
```
docker build -t liftango-challenge .
```

spin the whole thing up by running a command from the project's root directory 
```
docker-compose up -d
```

You can run this command to stop it:
```
docker-compose down
```


### Before starting
Before getting started solving the problem (defined below) make sure to take a look around the project and its structure. It has been purposfuly left sparse to allow you to feel free to structure your solution however you decide.

We have setup this project using [Spark](http://sparkjava.com/) as the web application framework. It is not expected that you will need much more than what has already been implemented. 

We also have a basic unit test setup with [JUnit](https://junit.org/) testing the very basic *Adder* class. 

Make sure you are able to run the single test and that it is passing.

Also make sure that you can run the application. It will listen on port 4567. 
Logging has not been specified and will default to no-op to keep this project simple.

To test the application you can run a simple `curl` request to localhost, e.g:
```$ curl localhost:4567/ping```


### Challenge Description

You have been provided with a simple web application. 
Your task is to modify the `/calc` endpoint and have it create a tower of 1s based on the input it is given on each request.

Each request should output a grid made of 8x8 0s and 1s in the form of a string, such as:
````
00000000
00000000
00000000
00000000
00000000
00010000
00110000
10111010
````
This string represents the current state of the application. 

You need to update the /calc endpoint to take an input number from the range 0 - 255 (modify the endpoint as needed).

Each row in the output represents one level in the tower of 1s. In the previous example, `10111010` is level zero of the tower. 

Before any request is made to the application, assume the grid is all zeros. 

Every request to the /calc endpoint builds the tower up by one level, starting from level zero. 

When sent a number, the bits that make up that number will correspond to the 1s and 0s used to build a new level of the tower. 

A new level must only have 1s placed in it where a 1 exists in the corresponding column below it. 

Assume that below level zero is all 1s.

If the first input given is 1, level zero will be output as `00000001`

Once the tower reaches the top of the grid, or is unable to create any new levels, the grid is to be reset to 0s and the tower building will start again.

You are free to implement as much or as little of this as you want.

If you do not have the time to complete any parts, you may write a description of how you would have completed that part.

If you have any questions or there are any ambiguities, feel free to ask for further details.

### Submitting
To submit this assessment, ideally we would like to see a pull request made to the repository with the changes you've made and an explanation of what/why you've implemented it in the way you have.

If you are unable to do this for any reason, feel free to email us with a zip of the entire repository including the `.git` directory. 


### Additional Questions
The following additional questions are to get a better insight into how you make software engineering decisions. 
We don't expect more than a short paragraph for each of these questions.
Please include these questions as part of your pull request or email.


1. Describe some techniques you would have/have used to mitigate concurrency issues with the solution you've developed. Identify potential issues and how to address them.

2. Describe how you could maintain the functionality of the solution if the application state was to be shared across multiple running instances of the web application (on the same computer/server). 

3. Would your answer to question 2 work if the application was running on different servers? If yes, why? If no, discuss what you could change and what systems/tools you may use to achieve that.

4. Describe some ways we could monitor the performance of this application and identify bottlenecks.

5. What information may we want to log (if any) for this application?



Good luck and we wish you the best!
