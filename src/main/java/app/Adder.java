package app;

public class Adder {

    private int counter = 0;

    public Adder() {}

    public int add(int number) {
        counter += number;
        return counter;
    }
}
